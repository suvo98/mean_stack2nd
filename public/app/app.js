angular.module('userApp',['appRoutes','userController','mainController','emailController','managementController','userServices','authServices','managementServices','ngAnimate','ui.bootstrap'])

.config(function($httpProvider){
   $httpProvider.interceptors.push('AuthInterceptors'); 
});

