angular.module('userServices',[])

.factory('User',function($http){
    
    var userFactory = {};
    
    userFactory.create = function(regData){
        return $http.post('/api/users',regData);
    }
    
//    User.checkUsername(regData);
    userFactory.checkUsername = function(regData){
        return $http.post('/api/checkUserName',regData);
    }
    
//    User.checkEmail(regData);
    userFactory.checkEmail = function(regData){
        return $http.post('/api/checkEmail',regData);
    }
    
//    User.activateAccount(token)
    
     userFactory.activateAccount = function(token){
        return $http.put('/api/activate/'+token);
    }
    
     userFactory.reNewSession = function(username){
        return $http.get('/api/reNewToken/'+username);
    }
    
     userFactory.getPermission = function(){
        return $http.get('/api/permission/');
    }
    
     userFactory.getUsers = function(){
        return $http.get('/api/management/');
    }
     
     // Delete a user
    userFactory.deleteUser = function(username) {
        return $http.delete('/api/management/' + username);
    };
    
    // Get user to then edit
    userFactory.getUser = function(id) {
        return $http.get('/api/edit/' + id);
    };
    
     // Edit a user
    userFactory.editUser = function(id) {
        return $http.put('/api/edit', id);
    };

    
    return userFactory;
    
})
