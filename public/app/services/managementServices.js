angular.module('managementServices',[])

.factory('User2',function($http){
    
    var managementFactory = {};
    
    managementFactory.create = function(regData){
        return $http.post('/api/users',regData);
    }
 
    
    
    return managementFactory;
    
})
