

angular.module('mainController',['userServices','authServices'])

.controller('mainCtrl',function($http,$location,$timeout,Auth,$rootScope,$window,$interval,$route,User,AuthToken){
    
    var app = this;
    app.loadme = false;
    
    app.checkSession = function(){
        if(Auth.isLoggedIn()){
            app.checkingSession = true;
            var interval = $interval(function(){
                
                var token = $window.localStorage.getItem('token');
                
                if(token === null){
                    $interval.cancel(interval);
                }else{
                    self.parsejwt = function(token){
                        var base64Url = token.split('.')[1];
                        var base64 = base64Url.replace('-','+').replace('_','/');
                        return JSON.parse($window.atob(base64));
                    }
                    var expireTime = self.parsejwt(token);
                    var timeStamp = Math.floor(Date.now()/1000);
                    var timeCheck = expireTime.exp - timeStamp;
                     if(timeCheck <= 25){
                         
                         showModal(1);
                         $interval.cancel(interval);
                         
                     }
                }
               
            },2000);
        }
    };
    
    app.checkSession();
    
    
    
    var showModal = function(option) {
			app.choiceMade = false; // Clear choiceMade on startup
			app.modalHeader = undefined; // Clear modalHeader on startup
			app.modalBody = undefined; // Clear modalBody on startup
			app.hideButton = false; // Clear hideButton on startup

			// Check which modal option to activate	(option 1: session expired or about to expire; option 2: log the user out)		
			if (option === 1) {
				app.modalHeader = 'Timeout Warning'; // Set header
				app.modalBody = 'Your session will expired in 30 minutes. Would you like to renew your session?'; // Set body
				$("#myModal").modal({ backdrop: "static" }); // Open modal
				// Give user 10 seconds to make a decision 'yes'/'no'
				$timeout(function() {
					if (!app.choiceMade) app.endSession(); // If no choice is made after 10 seconds, select 'no' for them
				}, 10000);
			} else if (option === 2) {
				app.hideButton = true; // Hide 'yes'/'no' buttons
				app.modalHeader = 'Logging Out'; // Set header
				$("#myModal").modal({ backdrop: "static" }); // Open modal
				// After 1000 milliseconds (2 seconds), hide modal and log user out
				$timeout(function() {
					Auth.logout(); // Logout user
					$location.path('/logout'); // Change route to clear user object
					hideModal(); // Close modal
				}, 2000);
			}
		};
    
    app.reNewSession = function(){
          app.choiceMade = true;

          User.reNewSession(app.username).then(function(data){
              if(data.data.success){
                  AuthToken.setToken(data.data.token);
                  app.checkSession();
                  
              }
              else{
                 app.modalBody =  data.data.message;


              }
              
          });
       $("#myModal").modal('hide');
        
    };  
    
    app.endSession = function(){
         app.choiceMade = true;
         hideModal();
        $timeout(function(){
            showModal(2);

         },2000) 
    }; 
    
    var hideModal = function(){
        $("#myModal").modal('hide');
    };   
    
    
    $rootScope.$on('$routeChangeStart',function(){
        if (!app.checkingSession) app.checkSession();
        
        if(Auth.isLoggedIn()){
           
            app.isLoggedIn = true;
            Auth.getUser().then(function(data){
                
                 User.getPermission().then(function(data){
                    if(data.data.permission === 'admin' || data.data.permission === 'moderator'){
                        app.authorize = true;
                        app.loadme = true;
                    } 
                     else{
                         app.authorize = false;
                          app.loadme = true;
                     }
                 });
                
            app.username = data.data.username;
            app.useremail = data.data.email;
            app.loadme = true;
            
                
           });
            
        
        }else{
           
            app.isLoggedIn = false;
            app.username = '';
            app.loadme = true;
        }
        
        if($location.hash() == '_=_'){
           $location.hash(null); 
        }
       
    });
    
    this.facebook = function(){
       $window.location = $window.location.protocol + '//' + $window.location.host + '/auth/facebook'; 
    };
    
    
   
    this.doLogin = function(loginData){
 
    Auth.login(app.loginData).then(function(data){
    
         app.loading = true;
         app.errorMsg = false;

        if(data.data.success){

            app.loading = false;
            app.successMsg = data.data.message;

            $timeout(function(){
                $location.path('/');
                app.loginData = null;
                app.successMsg = null;
                app.checkSession(); 
            },2000);

        }else{

            app.loading = false;
            app.errorMsg = data.data.message;

        }
    
      });
    };
    
    app.logout = function(){
        showModal(2);
    };
    
});