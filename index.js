var express  = require('express');
var app      = express();
var morgan   = require('morgan');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var router = express.Router();
var appRoutes = require('./app/routes/api')(router);
var path = require('path');
var passport = require('passport');
var social = require('./app/passport/passport.js')(app,passport);

app.use(morgan('dev'));
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: true })); 
app.use(express.static(__dirname + '/public'));
app.use('/api',appRoutes);

mongoose.connect('mongodb://localhost:27017/mycrud1', { useMongoClient: true })
//mongoose.Promise = global.Promise;
//mongoose.connect('mongodb://localhost:27017/mycrud1', { useMongoClient: true });


app.get('*',function(req,res){
   res.sendFile(path.join(__dirname + '/public/app/views/index.html')); 
});

app.listen(process.env.PORT || 8000,function(){
    console.log('Running the server at port 8000');
});