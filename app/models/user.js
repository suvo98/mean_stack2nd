var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');
var titlize = require('mongoose-title-case');
var validate = require('mongoose-validator');



var nameValidator = [
  validate({
    validator: 'matches',
    arguments: /^([a-zA-Z]{3,20})$/,
    message: 'Letter Must Be 3 to 20 Character And Only Letter And No Special Character'
  }),
 
];

var passwordValidator = [
  validate({
    validator: 'matches',
    arguments: /^(?=(?:[^A-Z]*[A-Z]){2})(?=[^!@#$&*]*[!@#$&*])(?=(?:[^0-9]*[0-9]){2}).{8,}$/,
    message: 'At least two uppercase, At least one special, At least two digit ,Password length is 8 or more'
  }),
 
];

var emailValidator = [
  validate({
    validator: 'isEmail',
    message: 'Is Not A Valid Email'
  }),
  validate({
    validator: 'isLength',
    arguments: [3,25],
    message: 'Email Should Be Between {ARGS[0]} And  {ARGS[1]} Characters'
  }),
 
];

var usernameValidator = [
  validate({
    validator: 'isAlphanumeric',
    message: 'Username only letter or number'
  }),
  validate({
    validator: 'isLength',
    arguments: [3,8],
    message: 'Email Should Be Between {ARGS[0]} And  {ARGS[1]} Characters'
  }),
 
];

var UserSchema = new Schema({
    name: {type: String, required: true, validate: nameValidator },
    username: {type: String, lowercase: true, required: true ,unique: true ,validate: usernameValidator },
    password: {type: String, required: true , validate: passwordValidator},
    email: {type: String, lowercase: true, required: true , unique: true , validate: emailValidator},
    active: {type: Boolean, required: true , default: false},
    temporarytoken: {type: String, required: true },
    permission: {type: String, required: true , default: 'admin'}
});

UserSchema.pre('save', function(next) {
  var user = this;
    bcrypt.hash(user.password, null, null, function(err, hash) {
     if(err) return next(err);
        user.password = hash;
        next();
  });
 
});


UserSchema.plugin(titlize, {
  paths: [ 'name' ], 
  trim: true
});


UserSchema.methods.comparePassword = function(password){
  return bcrypt.compareSync(password,this.password)  
};


module.exports = mongoose.model('User',UserSchema);



