var User     = require('../models/user');
var jwt = require('jsonwebtoken');
var secret = 'pagli';
var nodemailer = require('nodemailer');
var sgTransport = require('nodemailer-sendgrid-transport');

module.exports = function(router){
    
//    nodemailer-sendgrid-transport
        

        var options = {
          auth: {
            api_user: 'meanstackman',
            api_key: 'Password123@'
          }
        }

        var client = nodemailer.createTransport(sgTransport(options));


    
    
//  user registration    
    
    router.post('/users',function(req,res){
    var user = new User();
    user.username = req.body.username;
    user.password = req.body.password;
    user.email = req.body.email;
    user.name = req.body.name;
    user.temporarytoken = jwt.sign({ username:user.username ,email:user.email },secret,{ expiresIn:'24h' });
    if(req.body.username == null || req.body.username == '' || req.body.password == null || req.body.password == '' || req.body.email == null || req.body.email == '' || req.body.name == '' || req.body.name == null){
        
        res.json({ success: false , message: "Ensure your username or email that you provided !" });
    }else{
            user.save(function(err){
            if(err){
                
                if(err.errors != null){
                      if(err.errors.name){
                            res.json({ success: false , message: err.errors.name.message });
                        }  
                        else if(err.errors.username){
                            res.json({ success: false , message: err.errors.username.message });
                        } 

                        else if(err.errors.email){
                            res.json({ success: false , message: err.errors.email.message });
                        } 
                        else if(err.errors.password){
                            res.json({ success: false , message: err.errors.password.message });
                        }
                        else{
                            res.json({ success: false , message: err });  
                        }
                }
                else if(err){
                    if(err.code == '11000'){
                      res.json({ success: false , message: "Username Or Email Already Taken !" });
                    }else{
                      res.json({ success: false , message: err });   
                    }
                     
                }
            
            }
            else{
//                  var email = {
//                          from: 'localhost staff, staff@localhost.com',
//                          to: user.email,
//                          subject: 'Localhost Activation link',
//                          text: 'Hello world',
//                          html: '<b>Hello </b>' + user.name + ' , To activate your account please click below link <br><br>    <a href="http://localhost:8000/activate/'+user.temporarytoken+'">http://localhost:8000/activate</a>'
//                    };
//
//                  client.sendMail(email, function(err, info){
//                        if (err){
//                          console.log(err);
//                        }
//                        else {
//                          console.log('Message sent: ' + info.response);
//                        }
//                   });
               
                res.json({ success: true , message: "Account Registered Please Check Your Email And Active Your Account ." });
            }
        });
    }
    
  });
    
//    check username is exist
    
        
    router.post('/checkUserName',function(req,res){
       User.findOne({ username: req.body.username }).select('username').exec(function(err,user){

           if(err) throw err;
           if(user){
               res.json({ success:false, message:"Username Already Taken" });
           }else{
               res.json({ success:true, message:"Valid Username" });
           }
       })
    });
    
//    check email is exist
    
    router.post('/checkEmail',function(req,res){
       User.findOne({ email: req.body.email }).select('email').exec(function(err,user){

           if(err) throw err;
           if(user){
               res.json({ success:false, message:"Email Already Taken" });
           }else{
               res.json({ success:true, message:"Valid Email" });
           }
       })
    });
    
    
//    user authenticate login
    
    router.post('/authenticate',function(req,res){
       User.findOne({ username: req.body.username }).select('email username password').exec(function(err,user){

           if(err) throw err;
           if(!user){
               res.json({ success:false, message:"Could Not Authenticate" });
           }else if(user){
              
               if(req.body.password){
                 var validate_password = user.comparePassword(req.body.password);  
                    if(!validate_password){
                       res.json({ success:false, message:"Could Not Authenticate Password" });  
                    }else{
                       var token = jwt.sign({ username:user.username ,email:user.email },secret,{ expiresIn:'1h' });
                       res.json({ success:true, message:"User Authenticate" , token:token });  
                    }
               }else{
                  res.json({ success:false, message:"No Password Provided" }); 
               }
               
              
           }
       })
    });
    
    router.put('/activate/:token',function(req,res){
        User.findOne({ temporarytoken: req.params.token },function(err,user){
            if(err) throw err;
            var token = req.params.token;
            
             jwt.verify(token,secret,function(err,decoded){
               if(err){
                   res.json({ success:false, message:"Activation Link Has Been Expired !" }); 
               }
               else if(!user){
                   res.json({ success:false, message:"Activation Link Has Been Expired !" });  
               }     
               else{
                   user.temporarytoken = false;
                   user.active = true;
                   user.save(function(err){
                       if(err){
                           console.log(err);
                       }
                       else{
                            var email = {
                            from: 'localhost staff, staff@localhost.com',
                            to: user.email,
                            subject: 'Localhost Activation link',
                            text: 'Hello world',
                            html: '<b>Hello </b>' + user.name + ' Your Account has been successfully activated'
                            };

                            client.sendMail(email, function(err, info){
                                if (err){
                                  console.log(err);
                                }
                                else {
                                  console.log('Message sent: ' + info.response);
                                }
                            });
                            res.json({ success:true, message:"Account is activate!" });  
                       }
                   })
               }
            });
            
            
        });
    });   
    
//    middleware
    
    router.use(function(req,res,next){
      var token = req.body.token || req.body.query || req.headers['x-access-token'];
       if(token){
           jwt.verify(token,secret,function(err,decoded){
               if(err){
                   res.json({ success:false, message:"Token Invalid" }); 
               }else{
                   req.decoded = decoded; 
                   next();
               }
           });
       }else{
         res.json({ success:false, message:"No Token Provided" });   
       }
   });  
    
//    token decoder route
    
   router.post('/me',function(req,res){
      res.send(req.decoded); 
   }); 
    
//   renew token
    router.get('/reNewToken/:username',function(req,res){
       
      User.findOne({username: req.params.username}).select().exec(function(err,user){
          if(err) throw err;
          if(!user){
              res.json({ success:false, message:"No user Was found"}); 

          }
          else{
             var newtoken = jwt.sign({ username:user.username ,email:user.email },secret,{ expiresIn:'1h' });
             res.json({ success:true, message:"User Authenticate" , token:newtoken });   

          }
      });
   }); 
    
   
//   permission
    
    router.get('/permission',function(req,res){
          User.findOne({username: req.decoded.username}).select().exec(function(err,user){
          if(err) throw err;
          if(!user){
              res.json({ success:false, message:"No user Was found"}); 

          }
          else{
              
             res.json({ success:true, message:"User Authenticate" , permission:user.permission });   

          }
        });
    });
    
//    get all user
    
    router.get('/management',function(req,res){
          User.find({},function(err,users){
             if(err) throw err;
             User.findOne({username: req.decoded.username},function(err,mainUser){
              if(err) throw err;
              if(!mainUser){
                  res.json({ success:false, message:"No user Was found"}); 

              }
              else{
                  if(mainUser.permission === 'admin' || mainUser.permission === 'moderator'){
                      if(!users){
                         res.json({ success:false, message:"User Not Found!"});  
                      }else{
                          res.json({ success: true, users: users , permission: mainUser.permission });  
                      }
                  }else{
                     res.json({ success:false, message:"Insufficient Permission !"});  
                  }

                   

              }
            });
        });
    });
    
    
   // Route to delete a user
    router.delete('/management/:username', function(req, res) {
        var deletedUser = req.params.username; // Assign the username from request parameters to a variable
        User.findOne({ username: req.decoded.username }, function(err, mainUser) {
            if (err) throw err; // Throw error if cannot connect
            // Check if current user was found in database
            if (!mainUser) {
                res.json({ success: false, message: 'No user found' }); // Return error
            } else {
                // Check if curent user has admin access
                if (mainUser.permission !== 'admin') {
                    res.json({ success: false, message: 'Insufficient Permissions' }); // Return error
                } else {
                    // Fine the user that needs to be deleted
                    User.findOneAndRemove({ username: deletedUser }, function(err, user) {
                        if (err) throw err; // Throw error if cannot connect
                        res.json({ success: true }); // Return success status
                    });
                }
            }
        });
    });
    
        // Route to get the user that needs to be edited
    router.get('/edit/:id', function(req, res) {
        var editUser = req.params.id; // Assign the _id from parameters to variable
        User.findOne({ username: req.decoded.username }, function(err, mainUser) {
            if (err) throw err; // Throw error if cannot connect
            // Check if logged in user was found in database
            if (!mainUser) {
                res.json({ success: false, message: 'No user found' }); // Return error
            } else {
                // Check if logged in user has editing privileges
                if (mainUser.permission === 'admin' || mainUser.permission === 'moderator') {
                    // Find the user to be editted
                    User.findOne({ _id: editUser }, function(err, user) {
                        if (err) throw err; // Throw error if cannot connect
                        // Check if user to edit is in database
                        if (!user) {
                            res.json({ success: false, message: 'No user found' }); // Return error
                        } else {
                            res.json({ success: true, user: user }); // Return the user to be editted
                        }
                    });
                } else {
                    res.json({ success: false, message: 'Insufficient Permission' }); // Return access error
                }
            }
        });
    });

    // Route to update/edit a user
    router.put('/edit', function(req, res) {
        var editUser = req.body._id; // Assign _id from user to be editted to a variable
        if (req.body.name) var newName = req.body.name; // Check if a change to name was requested
        if (req.body.username) var newUsername = req.body.username; // Check if a change to username was requested
        if (req.body.email) var newEmail = req.body.email; // Check if a change to e-mail was requested
        if (req.body.permission) var newPermission = req.body.permission; // Check if a change to permission was requested
        // Look for logged in user in database to check if have appropriate access
        User.findOne({ username: req.decoded.username }, function(err, mainUser) {
            if (err) throw err; // Throw err if cannot connnect
            // Check if logged in user is found in database
            if (!mainUser) {
                res.json({ success: false, message: "no user found" }); // Return erro
            } else {
                // Check if a change to name was requested
                if (newName) {
                    // Check if person making changes has appropriate access
                    if (mainUser.permission === 'admin' || mainUser.permission === 'moderator') {
         
                        User.findByIdAndUpdate(editUser, {$set:{name:newName}}, {new: true}, function(err, model){
                               if (err) {
                                        console.log(err); 
                                    } else {
                                        res.json({ success: true, message: 'Name has been updated!' }); 
                                    } 
                        });
                    }
                }

                // Check if a change to username was requested
                if (newUsername) {
                    // Check if person making changes has appropriate access
                    if (mainUser.permission === 'admin' || mainUser.permission === 'moderator') {
                        
                        User.findByIdAndUpdate(editUser, {$set:{username:newUsername}}, {new: true}, function(err, model){
                               if (err) {
                                        console.log(err); 
                                    } else {
                                        res.json({ success: true, message: 'Username has been updated!' }); 
                                    } 
                        });
                        
                    } else {
                        res.json({ success: false, message: 'Insufficient Permissions' }); // Return error
                    }
                }

                // Check if change to e-mail was requested
                if (newEmail) {
                    // Check if person making changes has appropriate access
                    if (mainUser.permission === 'admin' || mainUser.permission === 'moderator') {
                        
                        User.findByIdAndUpdate(editUser, {$set:{email:newEmail}}, {new: true}, function(err, model){
                               if (err) {
                                        console.log(err); 
                                    } else {
                                        res.json({ success: true, message: 'Email has been updated!' }); 
                                    } 
                        });
                        
                    } else {
                        res.json({ success: false, message: 'Insufficient Permissions' }); // Return error
                    }
                }

                // Check if a change to permission was requested
                if (newPermission) {
                    // Check if user making changes has appropriate access
                    if (mainUser.permission === 'admin' || mainUser.permission === 'moderator') {
                        // Look for user to edit in database
                        User.findOne({ _id: editUser }, function(err, user) {
                            if (err) throw err; // Throw error if cannot connect
                            // Check if user is found in database
                            if (!user) {
                                res.json({ success: false, message: 'No user found' }); // Return error
                            } else {
                                // Check if attempting to set the 'user' permission
                                if (newPermission === 'user') {
                                    // Check the current permission is an admin
                                    if (user.permission === 'admin') {
                                        // Check if user making changes has access
                                        if (mainUser.permission !== 'admin') {
                                            res.json({ success: false, message: 'Insufficient Permissions. You must be an admin to downgrade an admin.' }); // Return error
                                        } else {
                                          
                                            
                                            User.findByIdAndUpdate(editUser, {$set:{permission:newPermission}}, {new: true}, function(err, model){
                                               if (err) {
                                                console.log(err); 
                                                } else {
                                                res.json({ success: true, message: 'Permissions have been updated!' }); 
                                                } 
                                            });
                                            
                                            
                                        }
                                    } else {
                                        User.findByIdAndUpdate(editUser, {$set:{permission:newPermission}}, {new: true}, function(err, model){
                                               if (err) {
                                                console.log(err); 
                                                } else {
                                                res.json({ success: true, message: 'Permissions have been updated!' }); 
                                                } 
                                            });
                                    }
                                }
                                // Check if attempting to set the 'moderator' permission
                                if (newPermission === 'moderator') {
                                    // Check if the current permission is 'admin'
                                    if (user.permission === 'admin') {
                                        // Check if user making changes has access
                                        if (mainUser.permission !== 'admin') {
                                            res.json({ success: false, message: 'Insufficient Permissions. You must be an admin to downgrade another admin' }); // Return error
                                        } else {
                                            User.findByIdAndUpdate(editUser, {$set:{permission:newPermission}}, {new: true}, function(err, model){
                                               if (err) {
                                                console.log(err); 
                                                } else {
                                                res.json({ success: true, message: 'Permissions have been updated!' }); 
                                                } 
                                            });
                                        }
                                    } else {
                                       User.findByIdAndUpdate(editUser, {$set:{permission:newPermission}}, {new: true}, function(err, model){
                                               if (err) {
                                                console.log(err); 
                                                } else {
                                                res.json({ success: true, message: 'Permissions have been updated!' }); 
                                                } 
                                            });
                                    }
                                }

                                // Check if assigning the 'admin' permission
                                if (newPermission === 'admin') {
                                    // Check if logged in user has access
                                    if (mainUser.permission === 'admin') {
                                        user.permission = newPermission; // Assign new permission
                                        // Save changes
                                        user.save(function(err) {
                                            if (err) {
                                                console.log(err); // Log error to console
                                            } else {
                                                res.json({ success: true, message: 'Permissions have been updated!' }); // Return success
                                            }
                                        });
                                    } else {
                                        res.json({ success: false, message: 'Insufficient Permissions. You must be an admin to upgrade someone to the admin level' }); // Return error
                                    }
                                }
                            }
                        });
                    } else {
                        res.json({ success: false, message: 'Insufficient Permissions' }); // Return error

                    }
                }
            }
        });
    });

    
    return router;
}